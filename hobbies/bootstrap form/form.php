<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <script src="../../assets/bootstrap/js/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add hobbies form</h2>
    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Persons's Name">
        </div>
        <div class="form-group">
            <label for="Hobbies">Select hobbies:</label>
            <div class="checkbox">
                <label><input type="checkbox" value="Book Reading">Book Reading</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Singing">Singing</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Dancing">Dancing</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Reciting">Reciting</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Drawing">Drawing</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="Gardening">Gardening</label>
            </div>

        </div>
        <button type="submit" class="btn btn-default">Submit</button>
        <button type="submit" class="btn btn-default">Reset</button>
    </form>
</div>

</body>
</html>

