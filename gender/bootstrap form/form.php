<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <script src="../../assets/bootstrap/js/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Gender form</h2>
    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Persons's Name">
        </div>
        <div class="form-group">
            <label for="Gender">Select Gender:</label>
            <div class="radio">
                <label><input type="radio" name="optradio">Male</label>

                <label><input type="radio" name="optradio">Female</label>
            </div>

        </div>

        <button type="submit" class="btn btn-default">Submit</button>
        <button type="submit" class="btn btn-default">Reset</button>
    </form>
</div>

</body>
</html>

