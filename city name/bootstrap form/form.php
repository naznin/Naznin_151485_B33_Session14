<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <script src="../../assets/bootstrap/js/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add City Name </h2>
    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Persons's Name">
        </div>
        <div class="form-group">
            <label for="City Name">City Name:</label>
            <select class="form-control" id="City Name">
                <option>Dhaka</option>
                <option>Chittagong</option>
                <option>Comilla</option>
                <option>Sylhet</option>
            </select>

        </div>

        <button type="submit" class="btn btn-default">Submit</button>
        <button type="submit" class="btn btn-default">Reset</button>
    </form>
</div>

</body>
</html>

