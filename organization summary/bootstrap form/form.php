<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <script src="../../assets/bootstrap/js/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Organization Summary form</h2>
    <form>
        <div class="form-group">
            <label for="name">Oraganization name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter Organization Name">
        </div>
        <div class="form-group">
            <label for="Summary">Summary:</label>
            <textarea class="form-control" rows="5" id="Summary"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
        <button type="submit" class="btn btn-default">Reset</button>
    </form>
</div>

</body>
</html>

