<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <script src="../../assets/bootstrap/js/jquery.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>booktitle add form</h2>
    <form>
        <div class="form-group">
            <label for="text">Book Title:</label>
            <input type="text" class="form-control" id="email" placeholder="Enter Book Title">
        </div>
        <div class="form-group">
            <label for="text">Author Name:</label>
            <input type="text" class="form-control" id="pwd" placeholder="Enter Author Name">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>

